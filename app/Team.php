<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'name',
        'group_id',
    ];
    
    public function group()
    {
        return $this->belongsTo('App\Group');
    }
    
    /** matches of A teams */
    public function matches()
    {
        return $this->hasMany('App\Match');
    }
    
    /** matches of B teams */
    public function matches_b()
    {
        return $this->hasMany('App\Match', 'team_b_id');
    }
}
