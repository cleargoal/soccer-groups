<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Team;
use App\Match;
use Illuminate\Support\Facades\DB;
use Exception;

class ApiController extends Controller
{
    protected $newModel, $allTeams;
    /** GROUPS */
    /**
     * Create a new flight instance.
     * @param Request $request
     * @return mixed
     */
    public function createGroup(Request $request)
    {
        $newGroup = new Group();
        $newGroup->name = $request->name;
        $newGroup->save();
        return view('created', ['title' => 'Создана группа', 'name' => $newGroup->name]);
    }
    
    // show groups list
    public function showGroups()
    {
        $items_list = Group::with('teams')->get();
        $items_list->each(function ($item, $key) {
            $item->hasChildren = $item->teams->count();
        });
        return view('items_list',
            [
                'title' => 'Все группы',
                'items_list' => $items_list,
                'remark' => 'Клик на имя группы - вывод списка команд группы',
                'entity' => 'group',
                'group_id' => '',
            ]);
    }
    
    // show teams list in group
    public function showGroup(Request $request)
    {
        $team_a = Team::where('group_id', $request->group_id)->with('matches')->get();
        $team_b = Team::where('group_id', $request->group_id)->with('matches_b')->get();
        $items_list = $team_a->merge($team_b);
        $items_list->each(function ($item, $key) {
            $item->hasChildren = $item->matches->merge($item->matches_b)->count();
        });
        return view('items_list',
            [
                'title' => 'Команды группы: ' . $request->group_name,
                'items_list' => $items_list,
                'remark' => 'Клик на имя команды - вывод списка матчей',
                'entity' => 'team',
                'group_id' => $request->group_id,
            ]);
    }
    
    // edit just group name
    public function updateGroup(Request $request)
    {
        DB::table('groups')
            ->where('id', $request->id)
            ->update(['name' => $request->name]);
        return view('created', ['title' => 'Переименована группа', 'par_name' => $request->name, 'parLabel' => 'Новое имя: ', 'name' => '']);
    }
    
    public function deleteGroup(Request $request)
    {
        try {
            DB::table('groups')
                ->where('id', $request->id)
                ->delete();
            return view('created', ['title' => 'Группа удалена', 'name' => '']);
        } catch (Exception $e) {
            return view('created', ['title' => 'Запрет удаления Группы - есть команды.', 'name' => '']);
        }
    }
    
    /** TEAMS */
    public function createTeam(Request $request)
    {
        $newTeam = new Team();
        $newTeam->fill($request->toArray());
        $newTeam->save();
        $inGroup = Group::where('id', $newTeam->group_id)->first();
        return view('created', ['title' => 'Создана команда', 'name' => $newTeam->name, 'par_name' => $inGroup->name, 'parLabel' => 'В группе']);
    }
    
    // shows all Teams if it has sense
    public function showTeams()
    {
        $items_list_a = Team::with('matches')->get();
        $items_list_b = Team::with('matches_b')->get();
    
        $items_list = $items_list_a->merge($items_list_b);
        $items_list->each(function ($item, $key) {
            $item->hasChildren = $item->matches->merge($item->matches_b)->count();
        });
        return view('items_list',
            [
                'title' => 'Все команды',
                'items_list' => $items_list,
                'remark' => 'Клик на имя команды - вывод списка матчей команды',
                'entity' => 'team',
                'group_id' => '',
            ]);
    }
    
    // show all matches of selected Team
    public function showTeam(Request $request)
    {
        $matches = Match::where('team_id', $request->team_id)->orWhere('team_b_id', $request->team_id)->get();
        $items_list = $this->getTeamsForMatchName($matches);
        return view('items_list',
            [
                'title' => 'Матчи команды: ' . $request->team_name,
                'items_list' => $items_list,
                'remark' => '',
                'entity' => 'match',
                'group_id' => $request->group_id,
            ]);
    }
    
    // edit just Team name
    public function updateTeam(Request $request)
    {
        DB::table('teams')
            ->where('id', $request->id)
            ->update(['name' => $request->name]);
        return view('created', ['title' => 'Переименована команда', 'par_name' => $request->name, 'parLabel' => 'Новое имя: ', 'name' => '']);
    }
    
    public function deleteTeam(Request $request)
    {
        try {
            DB::table('teams')
                ->where('id', $request->id)
                ->delete();
            return view('created', ['title' => 'Команда удален', 'name' => '']);
        } catch (Exception $e) {
            return view('created', ['title' => 'Запрет удаления Команды - есть матчи.', 'name' => '']);
        }
    }
    
    /** MATCHES */
    
    public function createMatch(Request $request)
    {
        $newMatch = new Match();
        $newMatch->fill($request->toArray());
        $newMatch->save();
        $getTeams = Team::whereIn('id', [$newMatch->team_id, $newMatch->team_b_id])->get();
        $newMatchName = $this->generateMatchName($newMatch, $getTeams);
        return view('created', ['title' => 'Создан матч', 'name' => $newMatchName]);
    }
    
    // show all matches
    public function showMatches()
    {
        $matches = Match::all();
        $items_list = $this->getTeamsForMatchName($matches);
        return view('items_list',
            [
                'title' => 'Все команды',
                'items_list' => $items_list,
                'remark' => 'Клик на имя команды - вывод списка матчей команды',
                'entity' => 'team',
                'group_id' => '',
            ]);
    }
    
    public function updateMatch(Request $request)
    {
        DB::table('matches')
            ->where('id', $request->id)
            ->update(['score_a' => $request->score_a, 'score_b' => $request->score_b]);
    
        $newMatch = Match::find($request->id);
        $getTeams = Team::whereIn('id', [$newMatch->team_id, $newMatch->team_b_id])->get();
        $newMatchName = $this->generateMatchName($newMatch, $getTeams);
        return view('created', ['title' => 'Исправлен счет матча', 'par_name' => '', 'parLabel' => 'Новый счет матча: ', 'name' => $newMatchName]);
    }
    
    public function deleteMatch(Request $request)
    {
        DB::table('matches')
            ->where('id', $request->id)
            ->delete();
        return view('created', ['title' => 'Матч удален', 'name' => '']);
    }
    
    protected function generateMatchName($newMatch, $getTeams)
    {
        $row = [];
        $names = $getTeams->map(function ($item) use ($newMatch, $row) {
            if ($item->id == $newMatch->team_id) {
                $row = ['team_id' => $newMatch->team_id, 'team_name' => $item->name, 'team_score' => $newMatch->score_a];
            } elseif ($item->id == $newMatch->team_b_id) {
                $row = ['team_id' => $newMatch->team_b_id, 'team_name' => $item->name, 'team_score' => $newMatch->score_b];
            }
            return $row;
        });
        return $names[0]['team_name'] . ' : ' . $names[1]['team_name'] . ', ' . $names[0]['team_score'] . ' : ' . $names[1]['team_score'];
    }
    
    protected function getTeamsForMatchName($items_list)
    {
        if (empty($this->allTeams)) {
            $this->allTeams = Team::all();
        }
        $allTeams = $this->allTeams;
        $items_list->each(function ($item, $key) use ($allTeams) {
            $fitTeams = $allTeams->whereIn('id', [$item->team_id, $item->team_b_id]);
            $newkey = 0;
            $getTeams = $fitTeams->keyBy(function ($item) use (&$newkey) {
                return $newkey++;
            });
            $item->name = $this->generateMatchName($item, $getTeams);
        });
        return $items_list;
    }
    
    // show page with result as demand
    public function showSpa()
    {
    
        return view('soccer_spa');
    }
    
    // show page with result as demand
    public function initSpa()
    {
        return view('soccer_spa');
    }
    
    public function creations(Request $request)
    {
        $ent = $request->entity;
        $operation = 'create';
        $group_id = $request->group_id ? $request->group_id : 0;
        $params = $this->setFormParams($ent, $operation, $group_id > 0 ? $group_id : null);
        $view = $ent == 'match' ? 'match' : 'forms';
        return view($view, ['params' => $params]);
    }
    
    public function editing(Request $request)
    {
        $ent = $request->entity;
        $operation = 'update';
        $params = $this->setFormParams($ent, $operation);
        $params['operation_label'] = 'редактирования';
        $params['operation'] = $operation;
        $params['data_list'] = $this->fillForm($ent);
        return view('editing', ['params' => $params]);
    }
    
    public function delete(Request $request)
    {
        $ent = $request->entity;
        $operation = 'delete';
        $params = $this->setFormParams($ent, $operation);
        $params['operation_label'] = 'удаления';
        $params['operation'] = $operation;
        $params['data_list'] = $this->fillForm($ent);
        return view('editing', ['params' => $params]);
    }
    
    protected function setFormParams($ent, $operation, $group_id = null)
    {
        $params['entity'] = $ent;
        $titles = [
            'group' => [
                'create' => 'Новая группа',
                'update' => 'Изменить имя группы',
                'delete' => 'Удалить 1 группу',
            ],
            'team' => [
                'create' => 'Новая команда',
                'update' => 'Изменить имя команды',
                'delete' => 'Удалить 1 команду',
            ],
            'match' => [
                'create' => $group_id !== null && $group_id > 0 ? 'Новый матч в группе' : 'Новый матч',
                'update' => 'Изменить матч',
                'delete' => 'Удалить 1 матч',
            ],
        ];
        $params['title'] = $titles[$ent][$operation];
    
        if ($operation == 'create') {
            switch ($ent) {
                case 'group':
                    $params['parent_label'] = '';
                    $params['parent_name'] = '';
                    $params['parent_list'] = '';
                    break;
    
                case 'team':
                    $params['parent_label'] = 'Добавить в группу';
                    $params['parent_name'] = 'group_id'; // id field name for foreign key in table
                    $params['parent_list'] = Group::select('id', 'name')->get()->toArray();
                    break;
    
                case 'match':
                    $params['parent_label'] = 'Команда';
                    if ($group_id !== null && $group_id > 0) {
                        $params['parent_list'] = Team::select('id', 'name')->where('group_id', $group_id)->get()->toArray();
                    } else {
                        $params['parent_list'] = Team::select('id', 'name')->get()->toArray();
                    }
                    break;
            }
        }
        return $params;
    }
    
    protected function fillForm($ent)
    {
        $className = 'App\\' . ucwords($ent);
        $object = new $className;
        if ($ent != 'match') {
            $names = $object->select('id', 'name')->get()->toArray(); // group & team
        } else {
            if (empty($this->allTeams)) {
                $this->allTeams = Team::all();
            }
            $allTeams = $this->allTeams;
            $matches = Match::all();
            $items_list = $this->getTeamsForMatchName($matches);
            $names = $items_list->map(function ($item) use ($allTeams) {
                $item->team_a_name = $allTeams->where('id', $item->team_id)->pluck('name')->toArray()[0];
                $item->team_b_name = $allTeams->where('id', $item->team_b_id)->pluck('name')->toArray()[0];
                return $item;
            })->toArray();
        }
        return $names;
    }
    
}
