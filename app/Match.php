<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'team_id',
        'team_b_id',
        'score_a',
        'score_b',
    ];
    
    public function team()
    {
        return $this->belongsTo('App\Team');
    }
}
