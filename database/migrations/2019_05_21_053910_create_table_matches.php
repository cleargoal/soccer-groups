<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatches extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->unsigned()->index();
            $table->integer('team_b_id')->unsigned()->index();
            $table->smallInteger('score_a')->unsigned()->default(0);
            $table->smallInteger('score_b')->unsigned()->default(0);
            $table->timestamps();
    
            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onDelete('RESTRICT');
            $table->foreign('team_b_id')
                ->references('id')->on('teams')
                ->onDelete('RESTRICT');
        });
    }
    
    
    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
