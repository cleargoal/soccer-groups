<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'create'], function () {
    Route::get('/group', 'ApiController@creations')->name('form-create-group');
    Route::get('/team', 'ApiController@creations')->name('form-create-team');
    Route::get('/match', 'ApiController@creations')->name('form-create-match');
});

Route::group(['prefix' => 'created'], function () {
    Route::post('/group', 'ApiController@createGroup')->name('create-group');
    Route::post('/team', 'ApiController@createTeam')->name('create-team');
    Route::post('/match', 'ApiController@createMatch')->name('create-match');
});

Route::group(['prefix' => 'show'], function () {
    Route::get('/groups', 'ApiController@showGroups')->name('show-groups');
    Route::get('/group', 'ApiController@showGroup')->name('show-group');
    Route::get('/teams', 'ApiController@showTeams')->name('show-teams');
    Route::get('/team', 'ApiController@showTeam')->name('show-team');
    Route::get('/matches', 'ApiController@showMatches')->name('show-matches');
});

Route::group(['prefix' => 'edit'], function () {
    Route::get('/group', 'ApiController@editing')->name('edit-group');
    Route::get('/team', 'ApiController@editing')->name('edit-team');
    Route::get('/match', 'ApiController@editing')->name('edit-match');
});

Route::group(['prefix' => 'update'], function () {
    Route::post('/group', 'ApiController@updateGroup')->name('update-group');
    Route::post('/team', 'ApiController@updateTeam')->name('update-team');
    Route::post('/match', 'ApiController@updateMatch')->name('update-match');
});

Route::group(['prefix' => 'delete'], function () {
    Route::get('/group', 'ApiController@delete')->name('form-delete-group');
    Route::get('/team', 'ApiController@delete')->name('form-delete-team');
    Route::get('/match', 'ApiController@delete')->name('form-delete-match');
});

Route::group(['prefix' => 'deleted'], function () {
    Route::post('/group', 'ApiController@deleteGroup')->name('delete-group');
    Route::post('/team', 'ApiController@deleteTeam')->name('delete-team');
    Route::post('/match', 'ApiController@deleteMatch')->name('delete-match');
});
