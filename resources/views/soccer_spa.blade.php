@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="table-borderless">
                    <div
                        class="table-responsive table-responsive-lg table-responsive-md table-responsive-sm table-responsive-sm table-responsive-xl px-4 px-lg-4 px-md-4 px-xl-4 px-sm-4 table-bordered">
                        <div class="table-row tab-pane">
                            <h1>Журнал футбольных групп</h1>
                        </div>
                        <div class="tab-content">
                            <div class="d-lg-table-row">
                                <div class="d-md-table-cell">Группы</div>
                                <div class="d-md-table-cell">Команды</div>
                                <div class="d-md-table-cell">Матчи</div>
                                {{--                                <div class="d-md-table-cell">Изменить</div>--}}
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
@endsection
