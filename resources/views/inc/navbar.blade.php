<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                {{--                <li class="nav-item">--}}
                {{--                    <a href="{{route('init-spa')}}" class="nav-link">Главная</a>--}}
                {{--                </li>--}}
                <li class="dropdown mr-xl-4">
                    <a class="dropdown-toggle" data-toggle="dropdown">Группы<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav-item">
                            <a href="{{route('form-create-group', ['entity' => 'group'])}}" class="nav-link">Новая группа</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('show-groups', ['entity' => 'group'])}}" class="nav-link">Список групп</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('edit-group', ['entity' => 'group'])}}" class="nav-link">Изменить имя группы</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('form-delete-group', ['entity' => 'group'])}}" class="nav-link">Удалить 1 группу</a>
                        </li>
                    </ul>
                </li>
    
                <li class="dropdown mr-xl-4">
                    <a class="dropdown-toggle" data-toggle="dropdown">Команды<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav-item">
                            <a href="{{route('form-create-team', ['entity' => 'team'])}}" class="nav-link">Новая команда</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('show-teams', ['entity' => 'team'])}}" class="nav-link">Все команды</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('edit-team', ['entity' => 'team'])}}" class="nav-link">Изменить имя команды</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('form-delete-team', ['entity' => 'team'])}}" class="nav-link">Удалить 1 команду</a>
                        </li>
                    </ul>
                </li>
    
                <li class="dropdown mr-xl-4">
                    <a class="dropdown-toggle" data-toggle="dropdown">Матчи<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="nav-item">
                            <a href="{{route('form-create-match', ['entity' => 'match'])}}" class="nav-link">Новый матч</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('show-matches')}}" class="nav-link">Все матчи</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('edit-match', ['entity' => 'match'])}}" class="nav-link">Исправить данные матча</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('form-delete-match', ['entity' => 'match'])}}" class="nav-link">Удалить 1 матч</a>
                        </li>
                    </ul>
                </li>
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
            </ul>
        </div>
    </div>
</nav>
