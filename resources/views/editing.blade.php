@extends('layouts.app')
@section('content')
    <div class="container">
        <fieldset class="row">
            <h1>{{$params['title']}}</h1>
            @php $route = $params['operation'].'-'.$params['entity']; @endphp
            <form action="{{route($route)}}" method="post">
                <fieldset>
                    @if(!empty($params['data_list']))
                        <label for="this_id">Выбрать для {{$params['operation_label']}}</label><br>
                        <select name="id" id="this_id" required onchange="setLabels(this.selectedOptions[0])">
                            @foreach($params['data_list'] as $data)
                                <option value="{{$data['id']}}"
                                        @if($params['entity'] == 'match')
                                        data-team-a="{{$data['team_a_name']}}"
                                        data-team-b="{{$data['team_b_name']}}"
                                    @endif
                                >{{$data['name']}}</option>
                            @endforeach
                        </select><br><br>
                        <script></script>
                        @if(!empty($params['operation'] && $params['operation'] == 'update'))
                            @if($params['entity'] != 'match')
                                <label for="new_name">Введите новое имя</label><br>
                                <input type="text" name="name" id="new_name" required>
                            @else
                                <label for="score_a" id="label_a" style="min-width: 120px;"></label>
                                <input type="number" name="score_a" id="score_a"><br>
                                <label for="score_b" id="label_b" style="min-width: 120px;"></label>
                                <input type="number" name="score_b" id="score_b"><br>
                            @endif
                        @endif
                    @endif
                    <br>
                    <input type="submit">
                </fieldset>
            </form>
        </fieldset>
    </div>
    <script>
        function setLabels(option) {
            let team_a = option.getAttribute('data-team-a');
            let team_b = option.getAttribute('data-team-b');
            let laba = document.getElementById('label_a');
            let labb = document.getElementById('label_b');
            laba.innerText = team_a;
            labb.innerText = team_b;
        }
    </script>
@endsection
