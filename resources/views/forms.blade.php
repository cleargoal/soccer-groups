@extends('layouts.app')
@section('content')
    <div class="container">
        <fieldset class="row">
            <h1>{{$params['title']}}</h1>
            @php $route = 'create-'.$params['entity']; @endphp
            <form action="{{route($route)}}" method="post">
                {{--                {{ csrf_field() }}--}}
                <fieldset>
                    <label for="name_id">Наименование</label><br>
                    <input type="text" name="name" id="name_id" required autofocus><br>
                    @if(!empty($params['parent_list']))
                        <label for="parent_id">{{$params['parent_label']}}</label><br>
                        <select name="{{$params['parent_name']}}" id="parent_id" required>
                            @foreach($params['parent_list'] as $parent)
                                <option value="{{$parent['id']}}">{{$parent['name']}}</option>
                            @endforeach
                        </select>
                    @endif
                    <br>
                    <input type="submit">
                </fieldset>
            </form>
        </div>
    </div>
@endsection
