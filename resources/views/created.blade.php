@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="table-borderless">
                    <div
                        class="table-responsive table-responsive-lg table-responsive-md table-responsive-sm table-responsive-sm table-responsive-xl px-4 px-lg-4 px-md-4 px-xl-4 px-sm-4 table-bordered">
                        <div class="table-row tab-pane">
                            <h1>{{$title}}</h1>
                        </div>
                        <div class="tab-content">
                            <div class="d-lg-table-row">
                                <div class="d-md-table-cell">{{$name}}</div>
                            </div>
                            @if(!empty($par_name))
                                <div class="d-lg-table-row">
                                    <div class="d-md-table-cell">{{$parLabel}}: {{$par_name}}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
@endsection
