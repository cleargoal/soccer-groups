@extends('layouts.app')
@section('content')
    <div class="container">
        <fieldset class="row">
            <h1>{{$params['title']}}</h1>
            @php $route = 'create-'.$params['entity']; @endphp
            <form action="{{route($route)}}" method="post">
                {{--                {{ csrf_field() }}--}}
                <fieldset>
                    @if(!empty($params['parent_list']))
                        <label for="parent_a">Команда А</label><br>
                        <select name="team_id" id="parent_a" required>
                            @foreach($params['parent_list'] as $parent)
                                <option value="{{$parent['id']}}">{{$parent['name']}}</option>
                            @endforeach
                        </select>
                        <label for="score-a">Счет-А</label>
                        <input type="number" id="score-a" name="score_a" min="0" max="50" required>
                        <br><br>
        
                        <label for="parent_b">Команда Б</label><br>
                        <select name="team_b_id" id="parent_b" required>
                            @foreach($params['parent_list'] as $parent)
                                <option value="{{$parent['id']}}">{{$parent['name']}}</option>
                            @endforeach
                        </select>
                        <label for="score-b">Счет-Б</label>
                        <input type="number" id="score-b" name="score_b" min="0" max="50" required>
                        <br>
                    @endif
                    <br>
                    <input type="submit">
                </fieldset>
            </form>
    </div>
    </div>
@endsection
