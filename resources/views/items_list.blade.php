@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="table-borderless">
                    <div
                        class="table-responsive table-responsive-lg table-responsive-md table-responsive-sm table-responsive-sm table-responsive-xl px-4 px-lg-4 px-md-4 px-xl-4 px-sm-4 table-bordered">
                        <div class="table-row tab-pane">
                            <h1>{{$title}}</h1>
                            @if(!empty($remark))
                                <h4 class="mb-4">{{$remark}}</h4>
                            @endif
                        </div>
                        <div class="tab-content">
                            @foreach($items_list as $item)
                                <div class="d-lg-table-row">
                                    @if($item->hasChildren > 0)
                                        <a href="{{route('show-'.$entity, [$entity.'_id' => $item->id, $entity.'_name' => $item->name])}}">
                                            @endif
                                        <div class="d-md-table-cell">{{$item->name}}</div>
                                            @if($item->hasChildren > 0)</a>@endif
                                </div>
                            @endforeach
                            @if($group_id && $items_list->count() > 1)
                                <br>
                                <h5>
                                    <a href="{{route('form-create-match', ['entity' => 'match', 'group_id' => $group_id])}}">
                                        Новый матч для команд этой группы
                                    </a>
                                </h5>
                            @endif
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
@endsection
